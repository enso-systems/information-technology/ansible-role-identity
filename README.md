<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-VERSION-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Identity

Ansible role to create/update/remove user accounts and provision ssh keys.

This role performs the following actions:

- Sets up user groups.
- Sets up user accounts. Generates ed25519 ssh keys for newly created accounts.
- Sets up `authorized_keys` for the user account.

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
